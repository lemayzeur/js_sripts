// Eextend dom to track form changes
// use: object.trackChanges or object.isChanged
$.fn.extend({
    trackChanges: function() {
        $(this.form).data("changed", false);
        $(":input",this).change(function() {
            $(this.form).data("changed", true);
        });
    },
    isChanged: function() { 
        return this.data("changed"); 
    }
});

// Convert HTML string to Object 
function xmlData(data){
    var el = document.createElement('html');
    el.innerHTML = "<html><head><title>titleTest</title></head><body>"+data+"</body></html>";
    return el;
}

// Submit form with buttons outside the target form
$(document).on("click",".submit-form",function(){
    $form = $(this).closest('form');
    $form.append("<input type='hidden' name='option' value='"+$(this).data('option')+"'>");
    $form.submit();
});

// Prevent users from cut/copy/past
// USE: just wrapper the content with the class .no-copy 
$(".no-copy").bind("copy paste cut",function(e){
    e.preventDefault(); return false;
});

$(document).on("click",'.data-link',function(){
    var url = $(this).data("href");
    if(url)window.location.href = url;
});

// add spin to button and disable event unitill action finished
// This will not work in Chrome
$(document).on("click",'.spin-wait',function(){
    $(this).attr('disabled',true);
    var classes =  $(this).find(".fa").attr("class");
    // $(this).find(".fa").remove();
    $(this).find(".fa").attr("class","fa fa-spin fa-spinner");
    // $(this).prepend("<i class='fa fa-spin fa-spinner'></i>");

    var together = $(this).attr("together");
    if(typeof(together) !== 'undefined'){
        $(together).each(function(i,e){
            $(e).attr('disabled',true).addClass("disabled");
        });
    }
});

// Display bytest in awesome formats: Kb, Mb, Gb
function c_bytes(number_of_bytes){
    if(isNaN(number_of_bytes)){
        return number_of_bytes;
    }
    if(number_of_bytes < 0)
        return number_of_bytes;
    var step_to_greater_unit = 1024.0;

    number_of_bytes = parseFloat(number_of_bytes);
    var unit = 'bytes';

    if((number_of_bytes / step_to_greater_unit) >= 1){
        number_of_bytes /= step_to_greater_unit;
        unit = 'Kb';
    }
    if((number_of_bytes / step_to_greater_unit) >= 1){
        number_of_bytes /= step_to_greater_unit;
        unit = 'Mb';
    }

    if((number_of_bytes / step_to_greater_unit) >= 1){
        number_of_bytes /= step_to_greater_unit;
        unit = 'Gb';
    }

    if((number_of_bytes / step_to_greater_unit) >= 1){
        number_of_bytes /= step_to_greater_unit;
        unit = 'Tb';
    }

    var precision = 2;
    
    number_of_bytes = number_of_bytes.toFixed(precision);
    
    return number_of_bytes + ' ' + unit;
}

// Show please wait dialog when clicking on a button
function showPleaseWait() {
    var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">\
        <div class="modal-dialog modal-sm center" style="top:40%;left:40%;position:fixed;color:white">\
            <span class="fa fa-circle-o-notch fa-spin fa-4x"></span>\
        </div>\
    </div>';
    $(document.body).append(modalLoading);
    $("#pleaseWaitDialog").modal("show");
}
$(".wait").click(function(){
    showPleaseWait();
});

// Get a specific cookie with a given name
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
// Set cookie with a given name,value, and its duration(Optional)
function setCookie(name,value,max_age=24*60*60) {
    var d = new Date();
    d.setTime(d.getTime() + (max_age*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}
// Delete a specific cookie with a given name
function deleteCookie(name) {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

// Update query URL without realoading page
// Pa two itilize, pi fo sit ki konstwi, reload paj yo totalman
function updateQuery(key,value){
    var url = [location.protocol, '//', location.host, location.pathname].join('');
    var query = window.location.search.substring(1);
    var query_object = {};
    var new_query = ''
    var j = 0

    if(query){
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            if(vars[i]){
                var pair = vars[i].split("=");
                if(typeof(pair[1]) === 'undefined'){
                    pair[1] = '';
                }
                query_object[pair[0]] = decodeURIComponent(pair[1]);
            }
        }
    }
    query_object[key] = value;

    for (var k in query_object){
        if(query_object[k])
            new_query = new_query + k + "=" + query_object[k];
        else
            new_query = new_query + k;
            
        if(j < Object.keys(query_object).length - 1){
            new_query += "&";
            j += 1;
        }
    }
    window.history.pushState("","","?"+new_query);

    $(".update-href").each(function(i,e){
        $(e).attr("href",$(e).attr("save-url")+new_query);
    });
    // return url + new_query
}



// PREVIEW a photo uploaded
$(document).on("change",".read-photo",function(e){ 
    $pr = $(this).data("pr");
    if (this.files && this.files[0]) {
        // don't rely on this only, check whether the file is correct in the server before you save it
        if(this.files[0].name.endsWith(".jpg") || this.files[0].name.endsWith(".jpeg") || this.files[0].name.endsWith(".png") || this.files[0].name.endsWith(".gif")){
            var reader = new FileReader();
            reader.onload = function (e) {

                $($pr).attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }else{
            alert("Format invalid: "+ this.files[0].name)
        }
    }
});

// Only number in inputs that have this class
$(".only_number").bind('keypress', function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});

// Only number with tirè in inputs that have this class
$(document).on('keypress',".only_number_with_tiret",function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9 || e.which == 45){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});

// Only number with dot in inputs that have this class
$(document).on('keypress',".only_number_with_dot",function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9 || e.which == 46){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});

// Only number with colon in inputs that have this class
$(document).on('keypress',".only_number_with_colon",function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9 || e.which == 58){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});

// Only number with plus in inputs that have this class
$(".only_number_with_plus").bind('keypress', function(e){
    if (e.keyCode == 65 && e.ctrlKey) {
        return true;
    }
    if(e.keyCode == 8 || e.keyCode == 9 || e.which == 43){
        return true;
    }
    if(isNumberKey(e)){
        return true;
    }else{
        return false;
    }
});

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if((charCode < 48 || charCode > 57))
        return false;
    return true;
}